%
% T: periodo
% p: ancho del pulso
%
function lab1(T, p)

    Wo = 2*pi/T;
    figure(1);
    subplot(2,1,1);
    t=-13:0.01:13;
    %Tren de pulsos cuadrados.
    f=( (abs(mod(t,T)) <= T/2 | abs(mod(t,T)) >= T/2) & (abs(mod(t,T)) <= p/2 | T-abs(mod(t,T)) <= p/2)).*1;
    q = plot(t,f);
    set(q,'Color',[0 0 1],'LineWidth',2);
    grid on;
    axis([-13,13,-2,2]);
    title('Tren de pulsos cuadrados');
    xlabel('Tiempo');ylabel('Amplitud');
    subplot(2,1,2);
    n=-17:0.5:17;
    %Coeficientes Serie de Fourier.
    Cn= (p/T)*(sinc(n*Wo*(p/2)));
    e = stem(n*Wo,(Cn),'.');
    set(e,'Color',[1 0 0], 'linewidth',2 ,'linestyle','-');
    axis([-17,17,-0.2,0.6]);
    title('Coeficientes Serie de Fourier');
    xlabel(' w= n*Wo ');ylabel(' Cn ');

endfunction
