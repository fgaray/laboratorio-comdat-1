
% p: Ancho del pulso
% ti: Periodo del impulso. Cuanto tiempo pasa para que se repita
function pulso(T, p)
        t = -10:0.01:10;
        f = 1/T;
        % con la regla de 3, T -> 100%, p -> x, x desconocido 
        % x es el porcetanje que tiene que ser duty en la funcion
        % pero eso da 50 o 30 y debe ser 0.5 o 0.3 y por eso otra
        % division por 100
        % creo que esta bien por que si T es 3 y pongo ancho p=1,
        % esto da x = 0.333, osea el 33% de 3 da 1 (igual era obvio por 
        % que viene de la ec. que hice XD)
        duty = p*100/T/100;
        fcuadrada = square(2*pi*t*f,duty);
        plot(t,fcuadrada);
        grid;
        title('Señal Cuadrada');
        xlabel('Tiempo(t)');
        ylabel('Amplitud');
        axis([-10,10, -2, 2]);
endfunction
