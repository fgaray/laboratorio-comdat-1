
% p: ancho
% T: periodo
%Wo: una constante

function coef(p, T , Wo)

n=-17:0.5:17;
%Coeficientes Serie de Fourier.
Cn= (p/T)*(sinc(n*Wo*(p/2)));
e = stem(n*Wo,(Cn),'.');
set(e,'Color',[1 0 0],'LineWidth',2);
axis([-17,17,-0.2,0.6]);
title('Coeficientes Serie de Fourier');
xlabel(' w= n*Wo ');ylabel(' Cn ');

endfunction
